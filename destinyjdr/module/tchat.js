export async function chatListeners(html, data) {
    html.on("change", '.input-struct-health', async (event) => {
        let idOfMessage = data.message._id;
        let sourcetype = event.currentTarget.dataset.sourcetype;
        let source = event.currentTarget.dataset.source;
        let actor;
        let structowner;
        let message;
        let type = event.currentTarget.dataset.structtype;
        let item;
        let structhealth;
        let actorID = event.currentTarget.dataset.actor;
        let othervalue = event.currentTarget.dataset.othervalue;
        
        if(sourcetype == "actor") {
            //Source is an actor (Stasis Crystals)
            message = await ChatMessage.get(idOfMessage);
            const itemObject = game.actors.get(source);
            item = [];
            item.type = "stasis";
            structhealth = event.currentTarget.value;
            let dataForMessage = {
                actorID: actorID,
                item: item,
                itemObject: itemObject,
                itemID: source,
                structhealth: structhealth,
                structarmor: othervalue,
                actor: actor
            }
            const messageTemplate = "systems/destinyjdr/templates/chat/struct-card.html";
            const html = await renderTemplate(messageTemplate, dataForMessage);
            message.update({content:html});
        }
        if(sourcetype == "skill") {
            //Source is a skill (Barricades, Monoliths...)
            actor = game.actors.get(actorID);
            structowner = actor.items.get(source);
            message = await ChatMessage.get(idOfMessage);
            const itemObject = structowner;
            if(type == "monolith") {
                item = [];
                item.type = "monolith";
                structhealth = event.currentTarget.value;
            } else {
                item = actor.items.get(source);
                structhealth = event.currentTarget.value;
            }
            let dataForMessage = {
                actorID: actorID,
                item: item,
                itemObject: itemObject,
                itemID: source,
                structhealth: structhealth,
                structarmor: othervalue,
                actor: actor
            }
            const messageTemplate = "systems/destinyjdr/templates/chat/struct-card.html";
            const html = await renderTemplate(messageTemplate, dataForMessage);
            message.update({content:html});
        }
    });
    html.on("change", '.input-struct-armor', async (event) => {
        let idOfMessage = data.message._id;
        let sourcetype = event.currentTarget.dataset.sourcetype;
        let source = event.currentTarget.dataset.source;
        let actor;
        let structowner;
        let message;
        let type = event.currentTarget.dataset.structtype;
        let item;
        let structarmor;
        let actorID = event.currentTarget.dataset.actor;
        let othervalue = event.currentTarget.dataset.othervalue;
        
        if(sourcetype == "actor") {
            //Source is an actor (Stasis Crystals)
            message = await ChatMessage.get(idOfMessage);
            const itemObject = game.actors.get(source);
            item = [];
            item.type = "stasis";
            structarmor = event.currentTarget.value;
            let dataForMessage = {
                actorID: actorID,
                item: item,
                itemObject: itemObject,
                itemID: source,
                structhealth: othervalue,
                structarmor: structarmor,
                actor: actor
            }
            const messageTemplate = "systems/destinyjdr/templates/chat/struct-card.html";
            const html = await renderTemplate(messageTemplate, dataForMessage);
            message.update({content:html});
        }
        if(sourcetype == "skill") {
            //Source is a skill (Barricades, Monoliths...)
            actor = game.actors.get(actorID);
            structowner = actor.items.get(source);
            message = await ChatMessage.get(idOfMessage);
            const itemObject = structowner;
            if(type == "monolith") {
                item = [];
                item.type = "monolith";
                structarmor = event.currentTarget.value;
            } else {
                item = actor.items.get(source);
                structarmor = event.currentTarget.value;
            }
            let dataForMessage = {
                actorID: actorID,
                item: item,
                itemObject: itemObject,
                itemID: source,
                structhealth: othervalue,
                structarmor: structarmor,
                actor: actor
            }
            const messageTemplate = "systems/destinyjdr/templates/chat/struct-card.html";
            const html = await renderTemplate(messageTemplate, dataForMessage);
            message.update({content:html});
        }
    });
}

export async function MeleeAttack({
    actorID = null,
    type = null } = {}) {

    const actor = game.actors.get(actorID);
    let meleetype = parseInt(type);
    let damage = "1d4";
    let strengthlevel = actor.system.stats.strength.toString();
    if(meleetype == 2) {
        strengthlevel = actor.system.stats.intellect.toString();
    }
    let fixednumber = strengthlevel.substring(0,strengthlevel.length-1);
    if(fixednumber == "") {
        fixednumber = 0;
    }
    damage = damage+"+"+fixednumber;

    let meleeelement = "";
    if(meleetype == 2) {
        if(actor.system.subclass == "gunslinger" || actor.system.subclass == "knifejuggler" || actor.system.subclass == "sunbreaker" || actor.system.subclass == "devastator" || actor.system.subclass == "dawnblade") {
            meleeelement = "solar";
        }
        if(actor.system.subclass == "arcstrider" || actor.system.subclass == "striker" || actor.system.subclass == "codeofthemissile" || actor.system.subclass == "stormcaller" || actor.system.subclass == "chaosseeker") {
            meleeelement = "arc";
        }
        if(actor.system.subclass == "nightstalker" || actor.system.subclass == "infiltrator" || actor.system.subclass == "sentinel" || actor.system.subclass == "childofthevoid" || actor.system.subclass == "voidwalker") {
            meleeelement = "void";
        }
        if(actor.system.subclass == "revenant" || actor.system.subclass == "behemoth" || actor.system.subclass == "shadebinder") {
            meleeelement = "stasis";
        }
        if(actor.system.subclass == "threadrunner" || actor.system.subclass == "berserker" || actor.system.subclass == "broodweaver") {
            meleeelement = "strand";
        }
        if(actor.system.subclass == "hallowed" || actor.system.subclass == "pioneer" || actor.system.subclass == "adoubant") {
            meleeelement = "vertant";
        }
        if(actor.system.subclass == "successor" || actor.system.subclass == "lancer" || actor.system.subclass == "alchemage") {
            meleeelement = "offaxis";
        }
    }

    const messageTemplate = "systems/destinyjdr/templates/partials/weapon-chat.html";
    const weaponType = game.i18n.localize("destinyjdr.itemText.meleeattack");

    let error = true;

    let rollResult = new Roll(damage, actor);
    await rollResult.evaluate({async:true});

    //Check if there is crit. Be careful: d4 cannot crit.
    let critnumber = 0;
    rollResult.dice.forEach(function(die) {
        if(die.faces !== 4) {
            let faces = die.faces;
            let results = die.results;
            results.forEach(function(aresult) {
                if(aresult.result == faces) {
                    if(critnumber < faces) {
                        critnumber = faces;
                    }
                }
            });
        }
    });

    //If crit, get the crit bonus damage value
    let critdamage = 0;
    if(critnumber !== 0) {
        if(critnumber == 6) {
            critdamage = Math.floor(Math.random() * 4) + 1;
        } else {
            if(critnumber == 8) {
                critdamage = Math.floor(Math.random() * 6) + 1;
            } else {
                if(critnumber == 10) {
                    critdamage = Math.floor(Math.random() * 8) + 1;
                } else {
                    if(critnumber == 12) {
                        critdamage = Math.floor(Math.random() * 10) + 1;
                    } else {
                        if(critnumber == 20) {
                            critdamage = Math.floor(Math.random() * 12) + 1;
                        }
                    }
                }
            }
        }
    }

    rollResult.terms.forEach((item, index) => {
        if(item.results) {} else {
            rollResult.terms.splice(index, 1);
        }
    });

    rollResult.terms.forEach((item, index) => {
        if(item.results) {} else {
            rollResult.terms.splice(index, 1);
        }
    });

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        rollResult: rollResult,
        weaponType: weaponType,
        actorID: actor.id,
        perkactivated: false,
        fixednumber: fixednumber,
        critnumber: critnumber,
        critdamage: critdamage,
        error: error,
        isreroll1: false,
        isreroll2: false,
        group_position: -2,
        dice_position: -2,
        meleeattack: meleetype,
        meleeelement: meleeelement
    }
    let htmlContent = await renderTemplate(messageTemplate, messageData);
    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        flags: {
            attackmessage: {
                rollResult: rollResult,
                weaponType: weaponType,
                actorID: actor.id,
                perkactivated: false,
                fixednumber: fixednumber,
                critnumber: critnumber,
                critdamage: critdamage,
                error: error,
                isreroll1: false,
                isreroll2: false,
                group_position: -2,
                dice_position: -2,
                meleeattack: meleetype,
                meleeelement: meleeelement
            }
        },
        content: htmlContent
    }
    rollResult.toMessage(messageData2);
}

export async function itemShow({
    itemData = null,
    actorID = null } = {}) {

    const messageTemplate = "systems/destinyjdr/templates/partials/object-card.html";
    const html = await renderTemplate(messageTemplate, itemData);

    const theActor = game.actors.get(actorID);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: theActor
        }),
        content: html,
        rarity: itemData.rarity
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function itemShowMore({
    itemData = null,
    actorID = null } = {}) {

    let messageTemplate = null;

    if(itemData.type === "object") {
        messageTemplate = "systems/destinyjdr/templates/partials/object-card-more.html";
    }
    if(itemData.type === "weapon") {
        messageTemplate = "systems/destinyjdr/templates/partials/weapon-card-more.html";
    }
    if(itemData.type === "super") {
        messageTemplate = "systems/destinyjdr/templates/partials/object-card-more.html";
    }
    
    const html = await renderTemplate(messageTemplate, itemData);

    const theActor = game.actors.get(actorID);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: theActor
        }),
        content: html,
        rarity: itemData.rarity
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function armorBreakCheck({
    actorID = null,
    itemID = null,
    difficulty = difficulty,
    sauvDestrLabel = sauvDestrLabel } = {}) {

    const theActor = game.actors.get(actorID);

    let messageTemplate = "systems/destinyjdr/templates/partials/destruction-card.html";
    let rollFormula = "d20";

    let rollResult = new Roll(rollFormula, theActor);
    await rollResult.evaluate({async:true});
    let renderedRoll = await rollResult.render({ template: messageTemplate });

    const theItem = theActor.items.get(itemID);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: theActor
        }),
        item: theItem,
        rollResult: rollResult,
        difficulty: difficulty,
        sauvDestrLabel: sauvDestrLabel,
        renderedRoll: renderedRoll
    }

    let htmlContent = await renderTemplate(messageTemplate, messageData);
        
    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: theActor
        }),
        content: htmlContent
    }

    rollResult.toMessage(messageData2);
}

export async function charStoryShow({
    actor = null } = {}) {

    let info = "story";

    let data = {
        info: info,
        actor: actor
    }

    const messageTemplate = "systems/destinyjdr/templates/chat/character-infos-card.html";
    const html = await renderTemplate(messageTemplate, data);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        info: info,
        content: html
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function charDescShow({
    actor = null } = {}) {

    let info = "desc";

    let data = {
        info: info,
        actor: actor
    }

    const messageTemplate = "systems/destinyjdr/templates/chat/character-infos-card.html";
    const html = await renderTemplate(messageTemplate, data);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        info: info,
        content: html
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function useSkill({
    actorID = null,
    itemID = null,
    skillType = null,
    rollstat = null,
    rollmod = null } = {}) {

    let actor = game.actors.get(actorID);
    let item = actor.items.get(itemID);

    let messageTemplate;
    let rollResult;
    let first_value;
    let second_value;
    let dice_score;
    let iscritical_success;
    let iscritical_failure;
    let damageorheal1 = false;
    let damageorheal2 = false;
    let damagestat1 = false;
    let damagestat2 = false;
    let stattoadd1;
    let stattoadd2;
    let basedamage1;
    let basedamage2;
    let damageelement;

    if(skillType == "noroll") {

        messageTemplate = "systems/destinyjdr/templates/chat/skill-card.html";
    
        rollResult = new Roll("d20+d10", actor, {actorId:actor.id});
        await rollResult.evaluate({async:true});
    
        first_value = rollResult.terms[0].results[0].result;
        second_value = rollResult.terms[2].results[0].result;
    
        if(first_value == 20) {
            first_value = "";
        }
    
        if(second_value == 10) {
            second_value = 0;
        }
    
        dice_score = parseInt(first_value+""+second_value);
    
        iscritical_success = false;
        iscritical_failure = false;
    
        if(dice_score <= 9) {
            iscritical_success = true;
        }
        if(dice_score >= 190) {
            iscritical_failure = true;
        }
    
        if(first_value == "") {
            first_value = 20;
        }

        let data = {
            item: item,
            actor: actor,
            rollResult: rollResult,
            iscritical_success: iscritical_success,
            iscritical_failure: iscritical_failure,
            dice_score: dice_score,
            first_value: first_value,
            second_value: second_value
        }

        const html = await renderTemplate(messageTemplate, data);

        let messageData = {
            speaker: ChatMessage.getSpeaker({
                actor: actor
            }),
            content: html
        }
    
        const messageClass = getDocumentClass("ChatMessage");
    
        messageClass.create(messageData);

    } else {

        if(skillType == "damage1" || skillType == "simple2damage") {
            damageelement = item.system.damageelement;
            damagestat1 = item.system.damagestat1;
            basedamage1 = item.system.damagedices1+"d"+item.system.damagedicetype1;
            if(damagestat1 == "nothing") {
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1;
            }
            if(damagestat1 == "combatskill") {
                stattoadd1 = actor.system.stats.combatskill.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "aimingskill") {
                stattoadd1 = actor.system.stats.aimingskill.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "strength") {
                stattoadd1 = actor.system.stats.strength.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "dexterity") {
                stattoadd1 = actor.system.stats.dexterity.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "resilience") {
                stattoadd1 = actor.system.stats.resilience.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "intellect") {
                stattoadd1 = actor.system.stats.intellect.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "perception") {
                stattoadd1 = actor.system.stats.perception.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "mentalstrength") {
                stattoadd1 = actor.system.stats.mentalstrength.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "charisma") {
                stattoadd1 = actor.system.stats.charisma.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "technology") {
                stattoadd1 = actor.system.stats.technology.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "superpoints") {
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+actor.system.superpoints.value;
                stattoadd1 = actor.system.superpoints.value.toString();
            }
        }
        if(skillType == "damage2") {
            damageelement = item.system.damageelement;
            damagestat1 = item.system.damagestat1;
            basedamage1 = item.system.damagedices1+"d"+item.system.damagedicetype1;
            if(damagestat1 == "nothing") {
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1;
            }
            if(damagestat1 == "combatskill") {
                stattoadd1 = actor.system.stats.combatskill.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "aimingskill") {
                stattoadd1 = actor.system.stats.aimingskill.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "strength") {
                stattoadd1 = actor.system.stats.strength.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "dexterity") {
                stattoadd1 = actor.system.stats.dexterity.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "resilience") {
                stattoadd1 = actor.system.stats.resilience.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "intellect") {
                stattoadd1 = actor.system.stats.intellect.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "perception") {
                stattoadd1 = actor.system.stats.perception.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "mentalstrength") {
                stattoadd1 = actor.system.stats.mentalstrength.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "charisma") {
                stattoadd1 = actor.system.stats.charisma.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "technology") {
                stattoadd1 = actor.system.stats.technology.toString();
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+stattoadd1.substring(0,stattoadd1.length-1);
                stattoadd1 = stattoadd1.substring(0,stattoadd1.length-1);
            }
            if(damagestat1 == "superpoints") {
                damageorheal1 = item.system.damagedices1+"d"+item.system.damagedicetype1+"+"+actor.system.superpoints.value;
                stattoadd1 = actor.system.superpoints.value.toString();
            }
            damagestat2 = item.system.damagestat2;
            basedamage2 = item.system.damagedices2+"d"+item.system.damagedicetype2;
            if(damagestat2 == "nothing") {
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2;
            }
            if(damagestat2 == "combatskill") {
                stattoadd2 = actor.system.stats.combatskill.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "aimingskill") {
                stattoadd2 = actor.system.stats.aimingskill.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "strength") {
                stattoadd2 = actor.system.stats.strength.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "dexterity") {
                stattoadd2 = actor.system.stats.dexterity.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "resilience") {
                stattoadd2 = actor.system.stats.resilience.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "intellect") {
                stattoadd2 = actor.system.stats.intellect.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "perception") {
                stattoadd2 = actor.system.stats.perception.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "mentalstrength") {
                stattoadd2 = actor.system.stats.mentalstrength.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "charisma") {
                stattoadd2 = actor.system.stats.charisma.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "technology") {
                stattoadd2 = actor.system.stats.technology.toString();
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+stattoadd2.substring(0,stattoadd2.length-1);
                stattoadd2 = stattoadd2.substring(0,stattoadd2.length-1);
            }
            if(damagestat2 == "superpoints") {
                damageorheal2 = item.system.damagedices2+"d"+item.system.damagedicetype2+"+"+actor.system.superpoints.value;
                stattoadd2 = actor.system.superpoints.value.toString();
            }
        }

        if(rollstat == "CSstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-1.html";
        }
        if(rollstat == "ASstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-2.html";
        }
        if(rollstat == "STRstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-3.html";
        }
        if(rollstat == "DEXstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-4.html";
        }
        if(rollstat == "RESstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-5.html";
        }
        if(rollstat == "INTstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-6.html";
        }
        if(rollstat == "PERstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-7.html";
        }
        if(rollstat == "MSstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-8.html";
        }
        if(rollstat == "CHstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-9.html";
        }
        if(rollstat == "TECstat") {
            messageTemplate = "systems/destinyjdr/templates/chat/stat-check-10.html";
        }
    
        rollResult = new Roll("d20+d10", actor, {actorId:actor.id});
        await rollResult.evaluate({async:true});
    
        first_value = rollResult.terms[0].results[0].result;
        second_value = rollResult.terms[2].results[0].result;
    
        if(first_value == 20) {
            first_value = "";
        }
    
        if(second_value == 10) {
            second_value = 0;
        }
    
        dice_score = parseInt(first_value+""+second_value);
    
        iscritical_success = false;
        iscritical_failure = false;
    
        if(dice_score <= 9) {
            iscritical_success = true;
        }
        if(dice_score >= 190) {
            iscritical_failure = true;
        }
    
        if(first_value == "") {
            first_value = 20;
        }

        let data = {
            item: item,
            actor: actor,
            rollResult: rollResult,
            iscritical_success: iscritical_success,
            iscritical_failure: iscritical_failure,
            dice_score: dice_score,
            first_value: first_value,
            second_value: second_value,
            rollmod: rollmod,
            damageorheal1: damageorheal1,
            damageorheal2: damageorheal2,
            damagestat1: damagestat1,
            damagestat2: damagestat2,
            basedamage1: basedamage1,
            basedamage2: basedamage2,
            stattoadd1: stattoadd1,
            stattoadd2: stattoadd2,
            damageelement: damageelement,
            skillType: skillType
        }
    
        const html = await renderTemplate(messageTemplate, data);
    
        let messageData = {
            speaker: ChatMessage.getSpeaker({
                actor: actor
            }),
            content: html
        }
    
        rollResult.toMessage(messageData);
    }
}

export async function superInvoke({
    itemID = null,
    actorID = null } = {}) {

    const actor = game.actors.get(actorID);
    const item = actor.items.get(itemID);

    const img = item.img;
    const cost = item.system.cost;
    const element = item.system.element;
    const duration = item.system.duration;
    const description = item.system.description;
    const overview = item.system.overview;
    const doesdamage = item.system.doesdamage;
    const doeseffects = item.system.doeseffects;
    const damage1dicenum = item.system.damage1dicenum;
    const damage2dicenum = item.system.damage2dicenum;
    const damage1dicetype = item.system.damage1dicetype;
    const damage2dicetype = item.system.damage2dicetype;
    const damage1level = item.system.damage1level;
    const damage2level = item.system.damage2level;
    const effect = item.system.effect;
    let fixednumber1;
    let fixednumber2;

    let doestwodamagetypes = false;
    if(damage2dicenum && damage2dicetype && damage2dicenum != "" && damage2dicetype != "") {
        doestwodamagetypes = true;
    }

    let superdamage1;
    if(damage1level == "nothing") {
        fixednumber1 = "";
        superdamage1 = damage1dicenum+"d"+damage1dicetype;
    }
    if(damage1level == "combatskill") {
        fixednumber1 = actor.system.stats.combatskill.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.CSstat`);
    }
    if(damage1level == "aimingskill") {
        fixednumber1 = actor.system.stats.aimingskill.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.ASstat`);
    }
    if(damage1level == "strength") {
        fixednumber1 = actor.system.stats.strength.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.STRstat`);
    }
    if(damage1level == "dexterity") {
        fixednumber1 = actor.system.stats.dexterity.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.DEXstat`);
    }
    if(damage1level == "resilience") {
        fixednumber1 = actor.system.stats.resilience.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.RESstat`);
    }
    if(damage1level == "intellect") {
        fixednumber1 = actor.system.stats.intellect.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.INTstat`);
    }
    if(damage1level == "perception") {
        fixednumber1 = actor.system.stats.perception.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.PERstat`);
    }
    if(damage1level == "mentalstrength") {
        fixednumber1 = actor.system.stats.mentalstrength.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.MSstat`);
    }
    if(damage1level == "charisma") {
        fixednumber1 = actor.system.stats.charisma.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.CHAstat`);
    }
    if(damage1level == "technology") {
        fixednumber1 = actor.system.stats.technology.toString();
        fixednumber1 = fixednumber1.substring(0,fixednumber1.length-1);
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.TECstat`);
    }
    if(damage1level == "superpoints") {
        fixednumber1 = actor.system.superpoints.value;
        fixednumber1 = fixednumber1.toString();
        superdamage1 = damage1dicenum+"d"+damage1dicetype+"+SP";
    }

    let superdamage2;
    if(damage2level == "nothing") {
        fixednumber2 = "";
        superdamage2 = damage2dicenum+"d"+damage2dicetype;
    }
    if(damage2level == "combatskill") {
        fixednumber2 = actor.system.stats.combatskill.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.CSstat`);
    }
    if(damage2level == "aimingskill") {
        fixednumber2 = actor.system.stats.aimingskill.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.ASstat`);
    }
    if(damage2level == "strength") {
        fixednumber2 = actor.system.stats.strength.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.STRstat`);
    }
    if(damage2level == "dexterity") {
        fixednumber2 = actor.system.stats.dexterity.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.DEXstat`);
    }
    if(damage2level == "resilience") {
        fixednumber2 = actor.system.stats.resilience.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.RESstat`);
    }
    if(damage2level == "intellect") {
        fixednumber2 = actor.system.stats.intellect.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.INTstat`);
    }
    if(damage2level == "perception") {
        fixednumber2 = actor.system.stats.perception.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.PERstat`);
    }
    if(damage2level == "mentalstrength") {
        fixednumber2 = actor.system.stats.mentalstrength.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.MSstat`);
    }
    if(damage2level == "charisma") {
        fixednumber2 = actor.system.stats.charisma.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.CHAstat`);
    }
    if(damage2level == "technology") {
        fixednumber2 = actor.system.stats.technology.toString();
        fixednumber2 = fixednumber2.substring(0,fixednumber2.length-1);
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+"+game.i18n.localize(`destinyjdr.minstats.TECstat`);
    }
    if(damage2level == "superpoints") {
        fixednumber2 = actor.system.superpoints.value;
        fixednumber2 = fixednumber2.toString();
        superdamage2 = damage2dicenum+"d"+damage2dicetype+"+SP";
    }

    if(fixednumber1 == "") {
        fixednumber1 = 0;
    }
    if(fixednumber2 == "") {
        fixednumber2 = 0;
    }

    let data = {
        name: item.name,
        actorID: actorID,
        img: img,
        cost: cost,
        element: element,
        duration: duration,
        description: description,
        overview: overview,
        doesdamage: doesdamage,
        doeseffects: doeseffects,
        damage1dicenum: damage1dicenum,
        damage2dicenum: damage2dicenum,
        damage1dicetype: damage1dicetype,
        damage2dicetype: damage2dicetype,
        damage1level: damage1level,
        damage2level: damage2level,
        effect: effect,
        superdamage1: superdamage1,
        superdamage2: superdamage2,
        doestwodamagetypes: doestwodamagetypes,
        fixednumber1: fixednumber1,
        fixednumber2: fixednumber2
    }

    const messageTemplate = "systems/destinyjdr/templates/chat/super-invoke-card.html";
    const html = await renderTemplate(messageTemplate, data);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        content: html
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function superSkillDamage({
    actorID = null,
    img = null,
    name = null,
    element = null,
    dicedamage = null,
    fixeddamage = null,
    damagestat = null,
    type = null } = {}) {

    const actor = game.actors.get(actorID);

    let critnumber;
    let critdamage;

    let damage;
    let rollResult;

    fixeddamage = parseInt(fixeddamage);

    let error;
    if(dicedamage == "" || dicedamage == true) {
        error = false;
    } else {
        error = true;

        damage = dicedamage;

        if(damagestat != "nothing") {
            damage += "+"+fixeddamage;
        } else {
            fixeddamage = 0;
        }
    
        rollResult = new Roll(damage, actor);
        await rollResult.evaluate({async:true});

        //Check if there is crit. Be careful: d4 cannot crit.
        critnumber = 0;
        rollResult.dice.forEach(function(die) {
            if(die.faces !== 4) {
                let faces = die.faces;
                let results = die.results;
                results.forEach(function(aresult) {
                    if(aresult.result == faces) {
                        if(critnumber < faces) {
                            critnumber = faces;
                        }
                    }
                });
            }
        });

        //If crit, get the crit bonus damage value
        critdamage = 0;
        if(critnumber !== 0) {
            if(critnumber == 6) {
                critdamage = Math.floor(Math.random() * 4) + 1;
            } else {
                if(critnumber == 8) {
                    critdamage = Math.floor(Math.random() * 6) + 1;
                } else {
                    if(critnumber == 10) {
                        critdamage = Math.floor(Math.random() * 8) + 1;
                    } else {
                        if(critnumber == 12) {
                            critdamage = Math.floor(Math.random() * 10) + 1;
                        } else {
                            if(critnumber == 20) {
                                critdamage = Math.floor(Math.random() * 12) + 1;
                            }
                        }
                    }
                }
            }
        }

        rollResult.terms.forEach((item, index) => {
            if(item.results) {
            } else {
                rollResult.terms.splice(index, 1);
            }
        });

        rollResult.terms.forEach((item, index) => {
            if(item.results) {
            } else {
                rollResult.terms.splice(index, 1);
            }
        });
    }

    let data = {
        actorID: actorID,
        error: error,
        rollResult: rollResult,
        img: img,
        name: name,
        element: element,
        fixednumber: fixeddamage,
        critnumber: critnumber,
        critdamage: critdamage,
        isreroll1: false,
        isreroll2: false,
        group_position: -2,
        dice_position: -2,
        damagelevel: damagestat,
        meleeattack: type
    }
    const messageTemplate = "systems/destinyjdr/templates/chat/super-damage-card.html";
    const html = await renderTemplate(messageTemplate, data);
    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        flags: {
            attackmessage: {
                error: error,
                rollResult: rollResult,
                img: img,
                name: name,
                element: element,
                fixednumber: fixeddamage,
                critnumber: critnumber,
                critdamage: critdamage,
                isreroll1: false,
                isreroll2: false,
                group_position1: -2,
                dice_position1: -2,
                group_position2: -2,
                dice_position2: -2,
                damagelevel: damagestat,
                meleeattack: type
            }
        },
        content: html
    }
    if(rollResult) {
        rollResult.toMessage(messageData);
    }
}

export async function superEffect({
    actorID = null,
    img = null,
    name = null,
    element = null,
    effect = null } = {}) {

    const actor = game.actors.get(actorID);

    let data = {
        effect: effect,
        img: img,
        name: name,
        element: element
    }
    const messageTemplate = "systems/destinyjdr/templates/chat/super-effect-card.html";
    const html = await renderTemplate(messageTemplate, data);
    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        content: html,
        meleeattack: 0
    }
    const messageClass = getDocumentClass("ChatMessage");
    messageClass.create(messageData);
}

export async function createStruct({
    actorID = null,
    itemID = null,
    type = null } = {}) {

    const actor = game.actors.get(actorID);
    const itemObject = actor.items.get(itemID);
    let item;
    let structhealth;
    let structarmor;
    if(type == "stasis") {
        item = [];
        item.type = "stasis";
        structhealth = actor.system.health.frozen;
        structarmor = 5;
        actor.update({"system.health.frozen":0});
    } else {
        if(type == "monolith") {
            item = [];
            item.type = "monolith";
            structhealth = 50;
            structarmor = 5;
            actor.items.get(itemID).update({"system.structhealth":structhealth});
            actor.items.get(itemID).update({"system.structarmor":structarmor});
        } else {
            item = actor.items.get(itemID);
            structhealth = 0;
            structarmor = 0;
            item.update({"system.structhealth":structhealth});
            item.update({"system.structarmor":structarmor});
        }
    }

    let data = {
        actorID: actorID,
        item: item,
        itemObject: itemObject,
        itemID: itemID,
        structhealth: structhealth,
        structarmor: structarmor,
        actor: actor
    }
    const messageTemplate = "systems/destinyjdr/templates/chat/struct-card.html";
    const html = await renderTemplate(messageTemplate, data);
    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        content: html
    }
    const messageClass = getDocumentClass("ChatMessage");
    messageClass.create(messageData);
}

export async function reRollingTchatDice({
    messageID = null,
    actorID = null,
    group_position = null,
    dice_position = null,
    rollData = null,
    rollResult = null,
    rarity = null,
    weaponType = null,
    weaponElement = null,
    weaponPenetration = null,
    perkactivated = null,
    perkname = null,
    perkid = null,
    perk = null,
    fixednumber = null,
    critnumber = null,
    critdamage = null,
    error = null,
    array_of_rerolls_group = null,
    array_of_rerolls_dice = null,
    array_of_rerolls_dicevalue = null,
    array_of_rererolls = null,
    newRollResult = null,
    array_of_rerolls_infos = null,
    attacktype = null,
    crit1damage = null,
    crit1number = null,
    damage1level = null,
    element = null,
    fixednumber1 = null,
    img = null,
    name = null,
    rollResult1 = null,
    meleeattack = null,
    meleeelement = null,
    damagelevel = null } = {}) {

    const actor = game.actors.get(actorID);
    let messageTemplate;
    if(attacktype == "weapon") {
        messageTemplate = "systems/destinyjdr/templates/partials/weapon-chat.html";
    }
    if(attacktype == "super") {
        messageTemplate = "systems/destinyjdr/templates/chat/super-damage-card.html";
        rollResult = rollResult1;
    }

    let old_dicevalues = array_of_rerolls_dicevalue;

    array_of_rerolls_dicevalue = [];

    let old_dicevalues_counter = 0;
    let old_dicevalues_terms_counter = 0;
    let new_dicevalueterms = [];
    old_dicevalues.forEach(function(old_dicevalue) {

        new_dicevalueterms = [];
        old_dicevalues_terms_counter = 0;
        old_dicevalue.terms.forEach(function(old_dicevalueterm) {
            new_dicevalueterms[old_dicevalues_terms_counter] = {
                "faces": old_dicevalueterm.faces,
                "number": old_dicevalueterm.number,
                "results": old_dicevalueterm.results
            };
            old_dicevalues_terms_counter++;
        });
        array_of_rerolls_dicevalue[old_dicevalues_counter] = {
            "data": old_dicevalue.data,
            "options": old_dicevalue.options,
            "terms": new_dicevalueterms,
            "_formula": old_dicevalue._formula,
            "_total": old_dicevalue._total
        };

        old_dicevalues_counter++;
    });

    let faces_value;
    let roll_actualresult;
    let rolldiecounter = 0;
    rollResult.terms.forEach(function(group) {
        rolldiecounter = 0;
        faces_value = group.faces;
        if(!group.operator) {
            group.results.forEach(function(die) {
                roll_actualresult = die.result;
                group.results[rolldiecounter] = {
                    "faces": faces_value,
                    "result": roll_actualresult,
                    "group_position": group_position,
                    "dice_position": dice_position,
                    "array_of_rerolls_group": array_of_rerolls_group,
                    "array_of_rerolls_dice": array_of_rerolls_dice,
                    "array_of_rerolls_dicevalue": array_of_rerolls_dicevalue
                };
                rolldiecounter++;
            });
        }
    });

    rollResult._dice = [];

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        rollData: rollData,
        rollResult: rollResult,
        rarity: rarity,
        actorID: actorID,
        weaponType: weaponType,
        weaponElement: weaponElement,
        weaponPenetration: weaponPenetration,
        perkactivated: perkactivated,
        perkname: perkname,
        perkid: perkid,
        perk: perk,
        fixednumber: fixednumber,
        critnumber: critnumber,
        critdamage: critdamage,
        error: error,
        isreroll1: true,
        isreroll2: false,
        group_position: group_position,
        dice_position: dice_position,
        array_of_rerolls_group: array_of_rerolls_group,
        array_of_rerolls_dice: array_of_rerolls_dice,
        newRollResult: newRollResult,
        array_of_rerolls_infos: array_of_rerolls_infos,
        crit1damage: crit1damage,
        crit1number: crit1number,
        damage1level: damage1level,
        element: element,
        error2: false,
        fixednumber1: fixednumber1,
        img: img,
        name: name,
        meleeattack: meleeattack,
        meleeelement: meleeelement,
        damagelevel: damagelevel
    }
    let htmlContent = await renderTemplate(messageTemplate, messageData);
    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        flags: {
            attackmessage: {
                rollData: rollData,
                rollResult: rollResult,
                rarity: rarity,
                weaponType: weaponType,
                weaponElement: weaponElement,
                weaponPenetration: weaponPenetration,
                perkactivated: perkactivated,
                perkname: perkname,
                perkid: perkid,
                perk: perk,
                fixednumber: fixednumber,
                critnumber: critnumber,
                critdamage: critdamage,
                error: error,
                isreroll1: true,
                isreroll2: false,
                group_position: group_position,
                dice_position: dice_position,
                array_of_rerolls_group: array_of_rerolls_group,
                array_of_rerolls_dice: array_of_rerolls_dice,
                array_of_rerolls_dicevalue: array_of_rerolls_dicevalue,
                array_of_rererolls: array_of_rererolls,
                array_of_rerolls_infos: array_of_rerolls_infos,
                crit1damage: crit1damage,
                crit1number: crit1number,
                damage1level: damage1level,
                element: element,
                error2: false,
                fixednumber1: fixednumber1,
                img: img,
                name: name,
                meleeattack: meleeattack,
                meleeelement: meleeelement,
                damagelevel: damagelevel
            }
        },
        content: htmlContent
    }
    newRollResult.toMessage(messageData2);
    const message = game.messages.get(messageID);
    setTimeout(() => {
        message.delete();
    }, 1450);
}

export async function displayBuff(buff) {
    
    const messageTemplate = "systems/destinyjdr/templates/partials/effectbuff-card.html";
    const html = await renderTemplate(messageTemplate, buff);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            alias:game.user.name
        }),
        content: html
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function displayDebuff(debuff) {

    const messageTemplate = "systems/destinyjdr/templates/partials/effectdebuff-card.html";
    const html = await renderTemplate(messageTemplate, debuff);

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            alias:game.user.name
        }),
        content: html
    }

    const messageClass = getDocumentClass("ChatMessage");

    messageClass.create(messageData);
}

export async function applyBuff(buff, actor) {

    let applyTheBuff = actor.effects.filter(effect => effect.data.label === game.i18n.localize("destinyjdr.statusEffectsBuffs."+buff))[0];

    if(applyTheBuff){return;}

    let theBuff = {
        label:game.i18n.localize("destinyjdr.statusEffectsBuffs."+buff),
        icon: "systems/destinyjdr/assets/img/effects/"+buff+".svg",
        changes:[],
        duration: {
            rounds: 1
        }
    };

    applyTheBuff = ActiveEffect.create(theBuff, {parent: actor});
}

export async function applyDebuff(debuff, actor) {

    let applyTheDebuff = actor.effects.filter(effect => effect.data.label === game.i18n.localize("destinyjdr.statusEffectsDebuffs."+debuff))[0];

    if(applyTheDebuff){return;}

    let effectData = {
        label:game.i18n.localize("destinyjdr.statusEffectsDebuffs."+debuff),
        icon: "systems/destinyjdr/assets/img/effects/"+debuff+".svg",
        changes:[],
        duration: {
            rounds: 1
        }
    };

    applyTheDebuff = ActiveEffect.create(effectData, {parent: actor});
}

/*
export function onSocketReceived(data) {
    if(!game.user.isGM) {
        return;
    }
}
*/
