export function TaskCheck({
    actionValue = null,
    isTraitRoll = false,
    fatiguePoints = null } = {}) {
    let d4 = "1d4";
    let d8 = "1d8";
    /* Ici fait les calculs */
    let statRollFormula = "@damageValue";
    let traitRollFormula = "@damageValue";
    
    let rollFormula = isTraitRoll == "true" ? traitRollFormula : statRollFormula;
    if (isTraitRoll == "true") {
        rollFormula -= fatiguePoints*d8;
    } else {
        rollFormula -= fatiguePoints*d4;
    }
    let rollData = {
        actionValue: actionValue
    };

    let messageData = {
      speaker: ChatMessage.getSpeaker()
    }
    new Roll(rollFormula, rollData).roll().toMessage(messageData);
}

export async function StatCheck({
    actionValue = null,
    statType = null,
    actorData = null } = {}) {

    const messageTemplate = "systems/destinyjdr/templates/chat/stat-check-" + statType + ".html";

    let rollResult = new Roll("d20+d10", actorData, {actorId:actorData.id});
    await rollResult.evaluate({async:true});

    let first_value = rollResult.terms[0].results[0].result;
    let second_value = rollResult.terms[2].results[0].result;

    if(first_value == 20) {
        first_value = "";
    }

    if(second_value == 10) {
        second_value = 0;
    }

    let dice_score = parseInt(first_value+""+second_value);

    let iscritical_success = false;
    let iscritical_failure = false;

    if(dice_score <= 9) {
        iscritical_success = true;
    }
    if(dice_score >= 190) {
        iscritical_failure = true;
    }

    if(first_value == "") {
        first_value = 20;
    }

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actorData
        }),
        rollResult: rollResult,
        actionValue: actionValue,
        actorID: actorData._id,
        iscritical_success: iscritical_success,
        iscritical_failure: iscritical_failure,
        dice_score: dice_score,
        first_value: first_value,
        second_value: second_value
    }

    let htmlContent = await renderTemplate(messageTemplate, messageData);

    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: actorData
        }),
        content: htmlContent
    }

    rollResult.toMessage(messageData2);
}

/**
 * Returns a string of css classes given the loca and whether it's been hit or not.
 * @param  {Object} loca std object : can contain booleans 'crit' and/or 'armor' or none of them.
 * @param  {Boolean} isHit whether that loca has been it (default false).
 * 
 * @returns {String} list of css classes to be added to the <p> element (can be '').
 */
 function getLocaClasses(loca, isHit = false) {
    let cssClasses = '';
    if ( isHit ) {
      cssClasses = loca.crit === true ? 'crit bold' : 'bold';
    } else {
      cssClasses = loca.crit === true ? 'crit-no-bold' : '';
    }
    return loca.armor === true ? `${cssClasses} armor`: cssClasses;
}

export async function LocCheck(locData) {
    const race = CONFIG.destinyjdr.racesLoc[locData.raceNum];

    const template = "systems/destinyjdr/templates/partials/tchat-card.html";
  
    //roll the loca hit
    const locaRoll = await new Roll('1d10').evaluate({async: true});
    const rollResult = parseInt(locaRoll.result);
    
    //find out which loca has been hit :
    const locaHit = race.locaList.filter(loca => loca.range.includes(rollResult))[0];
  
    //prepare the templateData
    const templateData = {
      rollResult: rollResult,
      bodyClass: getLocaClasses(locaHit, true),
      raceNum: locData.raceNum,
      raceName:  game.i18n.localize(`destinyjdr.race.${locData.raceNum}`),
      locaHit: locaHit,
      localist: race.locaList.map( loca => {
        const isHit = loca.id === locaHit.id;
        return {
          cssClasses: getLocaClasses(loca, isHit),
          isHit: isHit,
          name: game.i18n.localize(`destinyjdr.loca.${loca.id}`)
        }
      })
    }
  
    const html = await renderTemplate(template, templateData);
  
    ChatMessage.create({
      speaker: ChatMessage.getSpeaker({
            alias:game.user.name
        }),
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      sound: CONFIG.sounds.dice,
      roll: locaRoll,
      content: html,
      user: game.user.id
    });
}

export async function weaponAttack({
    rollData = null,
    actorData = null } = {}) {
    const messageTemplate = "systems/destinyjdr/templates/partials/weapon-chat.html";
    let damage = rollData.damage;
    const rarity = rollData.rarity;
    const weaponType = rollData.weaponType;
    const weaponElement = rollData.weaponElement;
    const weaponPenetration = rollData.weaponPenetration;
    const perkactivated = rollData.perkactivated;
    let perkname = "";
    let perkid = "";
    let perk = "";
    if(perkactivated) {
        perkname = rollData.perkname;
        perkid = rollData.perkid;
        perk = rollData.perk;
    }

    //Check if damage string is correct for a roll
    let error = true;
    let isokroll = new RegExp("^[0-9d+-]+$").test(damage);
    let spliteddamages = damage.split("+");
    let fixednumber = 0;
    if(isokroll) {
        spliteddamages.forEach(function(part) {
            if(part.includes("d") == false) {
                fixednumber = fixednumber + parseInt(part);
            }
        });
    } else {
        error = false;
        damage = "0";
    }

    let rollResult = new Roll(damage, actorData);
    await rollResult.evaluate({async:true});

    //Check if there is crit. Be careful: d4 cannot crit.
    let critnumber = 0;
    rollResult.dice.forEach(function(die) {
        if(die.faces !== 4) {
            let faces = die.faces;
            let results = die.results;
            results.forEach(function(aresult) {
                if(aresult.result == faces) {
                    if(critnumber < faces) {
                        critnumber = faces;
                    }
                }
            });
        }
    });

    //If crit, get the crit bonus damage value
    let critdamage = 0;
    if(critnumber !== 0) {
        if(critnumber == 6) {
            critdamage = Math.floor(Math.random() * 4) + 1;
        } else {
            if(critnumber == 8) {
                critdamage = Math.floor(Math.random() * 6) + 1;
            } else {
                if(critnumber == 10) {
                    critdamage = Math.floor(Math.random() * 8) + 1;
                } else {
                    if(critnumber == 12) {
                        critdamage = Math.floor(Math.random() * 10) + 1;
                    } else {
                        if(critnumber == 20) {
                            critdamage = Math.floor(Math.random() * 12) + 1;
                        }
                    }
                }
            }
        }
    }

    rollResult.terms.forEach((item, index) => {
        if(item.results) {} else {
            rollResult.terms.splice(index, 1);
        }
    });

    rollResult.terms.forEach((item, index) => {
        if(item.results) {} else {
            rollResult.terms.splice(index, 1);
        }
    });

    let swordstrengthdamage = "";
    if(weaponType == "sword") {
        let actorstrength = String(game.actors.get(actorData.id).system.stats.strength);
        swordstrengthdamage = actorstrength.substring(0,actorstrength.length-1);
    }

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actorData
        }),
        rollData: rollData,
        rollResult: rollResult,
        rarity: rarity,
        actorID: actorData.id,
        weaponType: weaponType,
        weaponElement: weaponElement,
        weaponPenetration: weaponPenetration,
        perkactivated: perkactivated,
        perkname: perkname,
        perkid: perkid,
        perk: perk,
        fixednumber: fixednumber,
        critnumber: critnumber,
        critdamage: critdamage,
        error: error,
        isreroll1: false,
        isreroll2: false,
        group_position: -2,
        dice_position: -2,
        meleeattack: 0,
        swordstrengthdamage: swordstrengthdamage
    }
    let htmlContent = await renderTemplate(messageTemplate, messageData);
    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: actorData
        }),
        flags: {
            attackmessage: {
                rollData: rollData,
                rollResult: rollResult,
                rarity: rarity,
                weaponType: weaponType,
                weaponElement: weaponElement,
                weaponPenetration: weaponPenetration,
                perkactivated: perkactivated,
                perkname: perkname,
                perkid: perkid,
                perk: perk,
                fixednumber: fixednumber,
                critnumber: critnumber,
                critdamage: critdamage,
                error: error,
                isreroll1: false,
                isreroll2: false,
                group_position: -2,
                dice_position: -2,
                meleeattack: 0,
                swordstrengthdamage: swordstrengthdamage
            }
        },
        content: htmlContent
    }
    rollResult.toMessage(messageData2);
}

export async function initiativeCheck({
    dexterityValue = null,
    perceptionValue = null,
    actorData = null } = {}) {

    const actor = game.actors.get(actorData);

    let d10 = "d10";

    let dexNum = Math.floor(dexterityValue / 10);
    let perNum = Math.floor(perceptionValue / 10);

    const messageTemplate = "systems/destinyjdr/templates/chat/initiative-check.html";

    let initCheck = d10 + ` + ` + dexNum +  ` + ` + perNum;

    let rollResult = new Roll(initCheck, actor, {actorId:actor.id});
    await rollResult.evaluate({async:true});

    game.combat?.combatants?.filter(c => c.actor.id === actor.id).forEach(c => c.update({ initiative: rollResult.total }));

    let messageData = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        dexterityValue: dexterityValue,
        perceptionValue: perceptionValue,
        rollResult: rollResult
    }

    let htmlContent = await renderTemplate(messageTemplate, messageData);

    let messageData2 = {
        speaker: ChatMessage.getSpeaker({
            actor: actor
        }),
        content: htmlContent
    }

    rollResult.toMessage(messageData2);
}