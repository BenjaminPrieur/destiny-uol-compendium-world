export class WeaponTalentSheet extends FormApplication {

     /** @override */
     constructor(weapon) {
       super(weapon, {
         closeOnSubmit: false,
         submitOnChange: true,
         submitOnClose: true,
         title: weapon.name
        });
        this.weapon = weapon;
     }
   
     /** @override */
     static get defaultOptions() {
       return mergeObject(super.defaultOptions, {
         classes: ["destinyjdr", "sheet", "weapon", "weapontalent-sheet"],
         template: 'systems/destinyjdr/templates/sheets/weapon-talent-sheet.html',
         width: 480,
         height: 158
       });
     }
   
     /** @override */
     getData() {
       let sheetData = {};
   
       // The weapon's data
       const weapondata = this.weapon.toObject(false);
       sheetData.weapon = weapondata;
       sheetData = weapondata;
   
       return sheetData;
     }
   
     /** @inheritdoc */
     async _updateObject(event, formData) {
       if ( !this.object.id ) return;
       return this.object.update(formData);
     }
   }