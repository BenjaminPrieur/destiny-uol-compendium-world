export class SuperSheet extends FormApplication {

    /** @override */
    constructor(actor, name, img, effect, duration, damage, cost, elemental, description) {
      super(actor, {
        closeOnSubmit: false,
        submitOnChange: true,
        submitOnClose: true,
        title: name
       });
  
      this.actor = actor;
      this.name = name;
      this.img = img;
      this.effect = effect;
      this.duration = duration;
      this.damage = damage;
      this.cost = cost;
      this.elemental = elemental;
      this.description = description;
      this.audio = audio;
    }
  
    /** @override */
    static get defaultOptions() {
      return mergeObject(super.defaultOptions, {
        classes: ["destinyjdr", "sheet", "item"],
        template: 'systems/destinyjdr/templates/sheets/super-sheet.html',
        width: 520,
        height: 210
      });
    }
  
    /** @override */
    getData() {
      const sheetData = {};
  
      const actorData = this.actor.toObject(false);
      sheetData.actor = actorData;
      sheetData = actorData;
      sheetData.name = this.name;
      sheetData.img = this.img;
      sheetData.effect = this.effect;
      sheetData.duration = this.duration;
      sheetData.damage = this.damage;
      sheetData.cost = this.cost;
      sheetData.elemental = this.elemental;
      sheetData.description = this.description;
      sheetData.audio = this.audio;
  
      return sheetData;
    }
  
    /** @inheritdoc */
    async _updateObject(event, formData) {
      if ( !this.object.id ) return;
      return this.object.update(formData);
    }
  }