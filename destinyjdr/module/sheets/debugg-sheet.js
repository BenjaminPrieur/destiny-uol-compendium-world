import * as Tchat from "../tchat.js";

export class ActiveEffectsDebugg extends DocumentSheet {

  /** @override */
  constructor(actor) {
    super(actor, {
      closeOnSubmit: false,
      submitOnChange: true,
      submitOnClose: true,
      title: "ActiveEffects Debugg"
     });

    this.actor = actor;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["destinyjdr", "sheet", "character"],
      template: 'systems/destinyjdr/templates/sheets/debugg-sheet.html',
      width: 600,
      height: 400
    });
  }

  getDisplayBuffContextMenu() {
    return Object.keys(CONFIG.destinyjdr.buffs).map( buff => (
      {
        name: game.i18n.localize(`destinyjdr.statusEffectsBuffs.${buff}`),
        icon: '',
        callback: (element) => {
          Tchat.displayBuff(buff);
        }
      }
    ));
  }

  getDisplayDebuffContextMenu() {
    return Object.keys(CONFIG.destinyjdr.debuffs).map( debuff => (
      {
        name: game.i18n.localize(`destinyjdr.statusEffectsDebuffs.${debuff}`),
        icon: '',
        callback: (element) => {
          Tchat.displayDebuff(debuff);
        }
      }
    ));
  }

  getApplyBuffContextMenu() {
    let actor = this.actor;
    return Object.keys(CONFIG.destinyjdr.buffs).map( buff => (
      {
        name: game.i18n.localize(`destinyjdr.statusEffectsBuffs.${buff}`),
        icon: '',
        callback: (element) => {
          Tchat.applyBuff(buff, actor);
        }
      }
    ));
  }

  getApplyDebuffContextMenu() {
    let actor = this.actor;
    return Object.keys(CONFIG.destinyjdr.debuffs).map( debuff => (
      {
        name: game.i18n.localize(`destinyjdr.statusEffectsDebuffs.${debuff}`),
        icon: '',
        callback: (element) => {
          Tchat.applyDebuff(debuff, actor);
        }
      }
    ));
  }

  getApplyAlcoholContextMenu() {
    let actor = this.actor;
    return Object.keys(CONFIG.destinyjdr.alcoholEffects).map( alcoholEffect => (
      {
        name: game.i18n.localize(`destinyjdr.alcoholEffects.${alcoholEffect}`),
        icon: '',
        callback: (element) => {
          Tchat.applyAlcohol(alcoholEffect, actor);
        }
      }
    ));
  }

  /** @override */
  getData() {
    const sheetData = {};

    // The Actor's data
    const actorData = this.actor.toObject(false);
    sheetData.actor = actorData;

    return sheetData;
  }

  activateListeners(html) {
    html.find('button').click(this._onDebuggChange.bind(this));
    new ContextMenu(html, ".destinyjdr-displaybuff", this.getDisplayBuffContextMenu());
    new ContextMenu(html, ".destinyjdr-displaydebuff", this.getDisplayDebuffContextMenu());
    new ContextMenu(html, ".destinyjdr-applybuff", this.getApplyBuffContextMenu());
    new ContextMenu(html, ".destinyjdr-applydebuff", this.getApplyDebuffContextMenu());
    super.activateListeners(html);
  }

  _onDebuggChange(event) {
    event.preventDefault();
    event.stopPropagation();
    const buttonElem = event.currentTarget;

    if(buttonElem.classList.contains("destinyjdr-deleteeffect")) {
      const effectID = buttonElem.closest(".destinyjdr-activeeffects-div").dataset.effectId;
      this.actor.deleteEmbeddedDocuments('ActiveEffect', [effectID]);
    } else {
      if(buttonElem.classList.contains("destinyjdr-editeffect")) {
        const effectID = buttonElem.closest(".destinyjdr-activeeffects-div").dataset.effectId;
        const effect = this.actor.effects.get(effectID);
        effect.sheet.render(true);
      } else {

        if(buttonElem.classList.contains("destinyjdr-effect-paracausal")) {
          let paracausal = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.sheet.paracausal")))[0];
          if(!paracausal) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.sheet.paracausal"),
              icon: "systems/destinyjdr/assets/img/effects/paracausal.svg"
            };
            paracausal = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealcombatskill")) {
          let unrealcombatskill = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.combatskill")))[0];
          if(!unrealcombatskill) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.combatskill"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealcombatskill = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealaimingskill")) {
          let unrealaimingskill = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.aimingskill")))[0];
          if(!unrealaimingskill) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.aimingskill"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealaimingskill = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealstrength")) {
          let unrealstrength = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.strength")))[0];
          if(!unrealstrength) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.strength"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealstrength = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealdexterity")) {
          let unrealdexterity = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.dexterity")))[0];
          if(!unrealdexterity) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.dexterity"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealdexterity = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealresilience")) {
          let unrealresilience = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.resilience")))[0];
          if(!unrealresilience) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.resilience"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealresilience = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealintellect")) {
          let unrealintellect = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.intellect")))[0];
          if(!unrealintellect) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.intellect"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealintellect = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealperception")) {
          let unrealperception = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.perception")))[0];
          if(!unrealperception) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.perception"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealperception = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealmentalstrength")) {
          let unrealmentalstrength = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.mentalstrength")))[0];
          if(!unrealmentalstrength) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.mentalstrength"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealmentalstrength = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealcharisma")) {
          let unrealcharisma = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.charisma")))[0];
          if(!unrealcharisma) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.charisma"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealcharisma = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
        if(buttonElem.classList.contains("destinyjdr-effect-unrealtechnology")) {
          let unrealtechnology = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize("destinyjdr.unreal.technology")))[0];
          if(!unrealtechnology) {
            const effectData = {
              label:game.i18n.localize("destinyjdr.unreal.technology"),
              icon: "systems/destinyjdr/assets/img/effects/unreal.webp"
            };
            unrealtechnology = ActiveEffect.create(effectData, {parent: this.actor});
          }
        }
      }
    }
  }

  /** @inheritdoc */
  async _updateObject(event, formData) {
    if ( !this.object.id ) return;
    return this.object.update(formData);
  }
}